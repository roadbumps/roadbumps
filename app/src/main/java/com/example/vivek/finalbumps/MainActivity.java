package com.example.vivek.finalbumps;
import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.ohoussein.playpause.PlayPauseView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;


public class MainActivity extends AppCompatActivity{
    private static final int REQUEST_WRITE_STORAGE = 112;
    File sdcard, directory,file;
    FileOutputStream fos;
    FileInputStream fis;
    InputStreamReader isr;
    SensorEventListener sensorEventListener;
    SensorManager sm;
    Sensor sensor;
    float flt;
    String str;
    TextView x,y,z;
    Float xval,yval,zval,graphLastXValue=1f;
    boolean flag=false;
    RelativeLayout l;
    double noOfBumps =0.0;
    TextView bumps;
    private LineGraphSeries<DataPoint> mSeries;
    GraphView graph;
    private static final Random RANDOM = new Random();
    private LineGraphSeries<DataPoint> series;
    private int lastX = 0;

    public MainActivity() throws FileNotFoundException {
    }
    PlayPauseView playPauseButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//------------------------------------------------------------------
        playPauseButton = (PlayPauseView) findViewById(R.id.play_pause_view);
        bumps=(TextView)findViewById(R.id.tv1);
        graph = (GraphView) findViewById(R.id.graph);
        initGraph(graph);
 //-------------------------------------------------------------------------

    



        sm=(SensorManager)getSystemService(SENSOR_SERVICE);
        sensor=sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sdcard = Environment.getExternalStorageDirectory();
        directory = new File(sdcard.getAbsolutePath()+"/Bumpy/");
        directory.mkdir();
        file = new File(directory,"result.txt");
        l=(RelativeLayout)findViewById(R.id.layout);


        playPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               if(playPauseButton.isPlay()) {
                   playPauseButton.toggle();
                   try {
                       bumps.setTextColor(getResources().getColor(R.color.colorPrimary));
                       bumps.setText("Test Running...");
                       writeFile();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }

               }
               if(!playPauseButton.isPlay()){
                   bumps.setTextColor(getResources().getColor(R.color.green));
                   bumps.setText("Completed.");
                   sm.unregisterListener( sensorEventListener,sensor);
                   playPauseButton.toggle();

               }

            }
        });

        final Button analyse=(Button)findViewById(R.id.button2);
        analyse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    analysevVlues();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                flag=true;
                new Handler().postDelayed(this, 200);
            }
        }, 200);





        if(ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_WRITE_STORAGE);
            return;
        }
    }
    //------------------------------------------------WRITE FILE-------------------------------
    public void writeFile() throws IOException {
        fos=new FileOutputStream(file);
        sm.registerListener(sensorEventListener=new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                    str=String.format("%.5f",sensorEvent.values[0]);
                    xval = sensorEvent.values[0];
                    yval = sensorEvent.values[1];
                    zval = sensorEvent.values[2];

                if(flag) {
                    mSeries.appendData(new DataPoint(graphLastXValue,sensorEvent.values[1]), true, 22);
                    graphLastXValue++;
                    try {
                        fos.write(str.getBytes());
                        fos.write("\n".getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    flag=false;
                }
            }



            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        },sensor, SensorManager.SENSOR_DELAY_NORMAL);

    }


    //------------------------------------ANALYSE---------------------------------------------------
    public void analysevVlues() throws IOException {
        fis=new FileInputStream(file);
        InputStreamReader isr=new InputStreamReader(fis);
        BufferedReader bfr=new BufferedReader(isr);
        String str;
        float sum=0,max=-200,fltparse=0;
        int count=0,count2=0;
        while((str=bfr.readLine()) != null){
                fltparse=Float.parseFloat(str);
                if(max<fltparse)
                    max=fltparse;
                sum=sum+fltparse;
                count++;
        }
        float newMax=(sum/count);
        float firstAvg=newMax;
        double numi=0.0,num=0.0;
        sum=0;


        bfr=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        while((str=bfr.readLine()) != null){
            fltparse=Float.parseFloat(str);
            if(newMax<fltparse) {
                newMax = fltparse;
                sum = sum + fltparse;
                count2++;
            }
            numi = Math.pow(( fltparse - firstAvg), 2);
            num+=numi;


        }
        noOfBumps =Math.sqrt(num/count);

        float newAvg=sum/count2;
        float finalval=count2;
        sum=0;
        int count3=0;
        bfr=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        while((str=bfr.readLine()) != null){
            fltparse=Float.parseFloat(str);
            if(newAvg<fltparse) {
                newAvg = fltparse;
                sum=sum+newAvg;
                count3++;
            }
        }
        double z=0;
        String bmp="Bump Detected";
        String bmps="Bumps Detected";


        if(String.format("%.0f",noOfBumps).equals("1"))
            bumps.setText((String.format("%.0f",noOfBumps)) + " " + bmp);
        else
            bumps.setText((String.format("%.0f",noOfBumps)) + " " + bmps);


    }
    //--------------------------------onPause----------------------

    public void reset(View v){
       // TextView tmp=(TextView)v.findViewById(R.id.tv1);
        bumps.setText("hello");
    }

    //---------------------------------INIT GRAPH--------------------------------------------

    public void initGraph(GraphView graph) {
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-15);
        graph.getViewport().setMaxY(15);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(4);
        graph.getViewport().setMaxX(80);

        // enable scaling and scrolling
        graph.getViewport().setScalable(true);
        graph.getViewport().setScalableY(true);

        graph.getViewport().setScrollable(true); // enables horizontal scrolling
        graph.getViewport().setScrollableY(true); // enables vertical scrolling
        graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
        graph.getViewport().setScalableY(true);

        graph.getGridLabelRenderer().setLabelVerticalWidth(100);
        // first mSeries is a line
        mSeries = new LineGraphSeries<>();
        mSeries.setDrawDataPoints(true);
        mSeries.setDrawBackground(true);
        graph.addSeries(mSeries);

    }
    //-------------------------------------------------------






}
